### Mockup props

mockup-resolution: 2480 * 3508;


### Quick commands

* Generate PDF

```bash
wkhtmltopdf /mnt/c/www/cv/html-pdf/index.html /mnt/c/www/cv/html-pdf/cv.pdf
```


### Next steps

* Add generating PDF with using Symfony 5 app build with TDD and DDD methodology. Use [Snappy](https://github.com/KnpLabs/KnpSnappyBundle) symfony bundle for using `wkhtmltopdf` utility
